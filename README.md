# rake-protect

[![Build Status](https://gitlab.com/mtuchowski/rake-protect/badges/master/build.svg)](https://gitlab.com/mtuchowski/rake-protect/commits/master)
[![Coverage Report](https://gitlab.com/mtuchowski/rake-protect/badges/master/coverage.svg)](https://gitlab.com/mtuchowski/rake-protect/commits/master)
[![Gem Version](https://badge.fury.io/rb/rake-protect.svg)](https://badge.fury.io/rb/rake-protect)

When some gems are unavailable in an environment, for example on a production server, requiring them in a `Rakefile` will cause `LoadError` to be thrown. Wrap the require and task declaration in `Rake.protect` block to prevent that.

For example, let's create a sample `Gemfile` with one gem limited only to `test` group:

```ruby
source 'https://rubygems.org'

gem 'rake'
gem 'rake-protect'

# Test environment gems only.
group :test do
  gem 'rubocop'
end
```

Then, install the gems, without the `test` group:

```console
$ bundle install --without=test
```

Now, requiring the `rubocop` gem inside `Rakefile` will naturally raise a `LoadError`:

```ruby
require 'rubocop/rake_task' # This raises a LoadError.

RuboCop::RakeTask.new
```

Wrap the require and task declaration in `Rake.protect` to suppress the error and allow yourself to use other, available tasks:

```ruby
require 'rake-protect'

Rake.protect do
  require 'rubocop/rake_task'

  RuboCop::RakeTask.new
end
```

## Development

After checking out the repo, run `bin/install-dependencies` to install dependencies.

The project uses [Rake](http://rake.rubyforge.org/) to automate development tasks. Run `bin/run` without any arguments to see the full list.

### Using development console

`bin/run guard` command starts a live console that automates various common tasks (running tests, linting source code, etc.) in response to file system modifications. This feature is provided by [Guard](https://github.com/guard/guard) gem.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
Copyright (c) 2017 Marek Tuchowski. See [LICENSE](./LICENSE) for details.
