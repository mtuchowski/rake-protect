# frozen_string_literal: true

# Entry point of the application. Load dependencies and set up environment.

require 'pathname'

# Global constants.
APP_ROOT = File.expand_path('../../', Pathname.new(__FILE__).realpath) unless defined?(APP_ROOT)

# Set location of Gemfile for bundler setup.
ENV['BUNDLE_GEMFILE'] ||= File.expand_path('Gemfile', APP_ROOT)

# Set up gems listed in the Gemfile.
require 'rubygems'
require 'bundler/setup'
