# frozen_string_literal: true

require 'rake'
require 'rainbow'

# rubocop:disable Style/Documentation
module Rake
  # Wraps block in rescue clause to prevent raising an exception when some gems are unavailable
  # in an environment, for example on a production server.
  #
  # @example
  #   Rake.protect do
  #     require 'unavailable_gem'
  #     ..
  #   end
  def self.protect(&block)
    raise ArgumentError unless block

    block.call
  rescue LoadError => error
    return unless Rake.verbose == true

    if RUBY_VERSION >= '2.1'
      # :nocov:
      task = error.backtrace_locations.find do |location|
        location.path.match(/^.*\.rake$/)
      end
      # :nocov:
    end

    $stderr.puts Rainbow("Tasks#{(' in ' + task.path) if task} not available: #{error.message}").red
  end
end
