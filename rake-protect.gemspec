# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name    = 'rake-protect'
  spec.version = '2.0.1'

  spec.author   = 'Marek Tuchowski'
  spec.email    = 'marek@tuchowski.com.pl'
  spec.homepage = 'https://gitlab.com/mtuchowski/rake-protect'

  spec.license     = 'MIT'
  spec.summary     = 'Protect from raising an exception when requiring unavailable gems in Rakefile'
  spec.description = <<-DESCRIPTION
  When some gems are unavailable in an environment, for example on a production server, requiring
  them in a Rakefile will cause LoadError to be thrown. Wrap the require and task declaration
  in Rake.protect block to prevent that.
  DESCRIPTION

  spec.rdoc_options     = ['--charset', 'UTF-8']
  spec.extra_rdoc_files = ['README.md', 'LICENSE']

  spec.files         = Dir.glob('lib/**/*')
  spec.require_paths = ['lib']

  spec.required_ruby_version     = Gem::Requirement.new('>= 2.3.8')
  spec.rubygems_version          = '2.6.10'
  spec.required_rubygems_version = Gem::Requirement.new('>= 2.6')

  spec.add_dependency 'rake',    '>= 10.0'
  spec.add_dependency 'rainbow', '>= 3.0'

  spec.add_development_dependency 'bundler',           '~> 1.14'
  spec.add_development_dependency 'rspec',             '~> 3.5'
  spec.add_development_dependency 'rubocop',           '~> 0.47'
  spec.add_development_dependency 'rubocop-rspec',     '~> 1.5'
  spec.add_development_dependency 'reek',              '>= 3.11'
  spec.add_development_dependency 'guard',             '~> 2.14'
  spec.add_development_dependency 'guard-rake',        '~> 1.0'
  spec.add_development_dependency 'guard-rspec',       '~> 4.7'
  spec.add_development_dependency 'listen',            '~> 3.0'
  spec.add_development_dependency 'simplecov',         '~> 0.13'
  spec.add_development_dependency 'simplecov-console', '~> 0.4'
  spec.add_development_dependency 'simplecov-json',    '~> 0.2'
end
