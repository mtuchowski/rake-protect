# frozen_string_literal: true

require 'spec_helper'
require 'rake-protect'

RSpec.describe Rake do
  context '#protect' do
    it 'fails without arguments' do
      expect { described_class.protect }.to raise_error ArgumentError
    end

    it 'accepts block as argument' do
      expect { described_class.protect {} }.not_to raise_error
    end

    it 'invokes given block' do
      expect { |b| described_class.protect(&b) }.to yield_with_no_args
    end

    it 'stops LoadError propagation' do
      expect do
        described_class.protect do
          require 'unavailable_gem_with_stupid_name'
        end
      end.not_to raise_error
    end

    it 'does not block other errors' do
      expect do
        described_class.protect do
          throw StandardError
        end
      end.to raise_error StandardError
    end

    it 'does not output anything when Rake.verbose is not set' do
      result = expect do
        described_class.protect do
          require 'unavailable_gem_with_stupid_name'
        end
      end

      result.not_to output.to_stdout
      result.not_to output.to_stderr
    end

    context 'when Rake.verbose is set' do
      before { described_class.verbose(true) }

      it 'writes error to stderr' do
        expect do
          described_class.protect do
            require 'unavailable_gem_with_stupid_name'
          end
        end.to output(/Tasks not available:/).to_stderr
      end

      if RUBY_VERSION >= '2.1'
        it 'writes task path if possible' do
          expect do
            described_class.protect do
              load 'examples/failing_task.rake'
            end
          end.to output(%r{examples/failing_task.rake}).to_stderr
        end
      end
    end
  end
end
