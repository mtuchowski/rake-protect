# frozen_string_literal: true

# Make sure to require this file for all spec files. Conventionally, all specs live under
# a `spec` directory, which RSpec adds to the `$LOAD_PATH` so simple `require 'spec_helper'
# will work.
#
# Keep this file as light-weight as possible. Requiring heavyweight dependencies from this file
# will add to the boot time of test suite on EVERY test run, even for an individual file that may
# not need all of that loaded. Instead, make a separate helper file that requires the additional
# dependencies and performs the additional setup, and require it from the spec files that actually
# need it.

# Load SimpleCov for all specs.
require 'support/simplecov'

# See http://rubydoc.info/gems/rspec-core/RSpec/Core/Configuration
RSpec.configure do |config|
  # rspec-expectations config goes here.
  config.expect_with :rspec do |expectations|
    # This option makes the `description` and `failure_message` of custom matchers include text
    # for helper methods defined using `chain`, e.g.:
    #     be_bigger_than(2).and_smaller_than(4).description
    #     # => "be bigger than 2 and smaller than 4"
    # ...rather than:
    #     # => "be bigger than 2"
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true

    # Only allow expect syntax.
    expectations.syntax = :expect
  end

  # rspec-mocks config goes here.
  config.mock_with :rspec do |mocks|
    # Prevents from mocking or stubbing a method that does not exist on a real object.
    # This is generally recommended.
    mocks.verify_partial_doubles = true
  end

  # Limits the available syntax to the non-monkey patched syntax that is recommended.
  config.disable_monkey_patching!

  # Run specs in random order to surface order dependencies. To debug an order dependency after
  # finding one, fix the order by providing the seed, which is printed after each run.
  #     --seed 1234
  config.order = :random

  # Seed global randomization in this process using the `--seed` CLI option. Setting this allows
  # to use `--seed` to deterministically reproduce test failures related to randomization
  # by passing the same `--seed` value as the one that triggered the failure.
  Kernel.srand config.seed

  # Allows RSpec to persist some state between runs in order to support the `--only-failures`
  # and `--next-failure` CLI options.
  config.example_status_persistence_file_path = 'tmp/rspec.cache'

  # Allows to limit a spec run to individual examples or groups by tagging them with `:focus`
  # meta-data. When nothing is tagged with `:focus`, all examples get run. RSpec also provides
  # aliases for `it`, `describe`, and `context` that include `:focus` meta-data: `fit`, `fdescribe`
  # and `fcontext`, respectively.
  config.filter_run_when_matching :focus

  # Make shared context metadata to be inherited by the metadata hash of host groups and examples,
  # rather than triggering implicit auto-inclusion in groups with matching metadata.
  #
  # This option will default to `:apply_to_host_groups` in RSpec 4 (and will have no way to turn
  # it off).
  config.shared_context_metadata_behavior = :apply_to_host_groups
end
