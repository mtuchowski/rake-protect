# frozen_string_literal: true

# SimpleCov hook for RSpec.

require 'simplecov'
require 'simplecov-console'
require 'simplecov-json'

SimpleCov.configure do
  coverage_dir 'coverage'

  minimum_coverage(99)
  minimum_coverage_by_file(90)
end

module SimpleCov
  module Formatter
    # Formatter used for CI builds.
    class CIFormatter
      def format(result)
        puts "coverage: #{Kernel.format('%6.2f%%', result.covered_percent)}"
      end
    end
  end
end

if ENV['CI']
  SimpleCov.formatter = SimpleCov::Formatter::CIFormatter
elsif ENV['COVERAGE_REPORT']
  SimpleCov.formatter = SimpleCov::Formatter::HTMLFormatter
else
  SimpleCov.formatters = [
    SimpleCov::Formatter::Console,
    # Formatter used in code editor integrations.
    SimpleCov::Formatter::JSONFormatter
  ]
end

SimpleCov.start unless ENV['NO_COVERAGE']
