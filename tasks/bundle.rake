# frozen_string_literal: true

namespace :bundle do
  require 'bundler/gem_tasks'

  desc 'Check that gems requested in the Gemfile are installed'
  task :check do
    sh 'bundle check --dry-run'
  end
end
