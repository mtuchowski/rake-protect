# frozen_string_literal: true

task :default do
  # It would be faster to just invoke Rake.application.display_tasks_and_comments. But first,
  # it involves setting few options outside of the normal flow. And second, we still don't have
  # task descriptions because Rake::TaskManager.record_task_metadata was not set when we were
  # initializing.
  sh 'bin/run -T', verbose: false
end
