# frozen_string_literal: true

require 'guard/rake_task'
require 'guard/rake'

Guard::RakeTask.new

namespace :guard do
  desc 'Lists Guard plugins that can be used with init'
  task :list do
    sh 'bundle exec guard list'
  end

  desc 'Show all defined Guard plugins and their options'
  task :show do
    sh 'bundle exec guard show'
  end
end

# Prevent double loading of Rakefile
Guard::Rake.rakefile_loaded = true
