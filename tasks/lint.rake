# frozen_string_literal: true

require 'rubocop/rake_task'
require 'reek/rake/task'

namespace :lint do
  RuboCop::RakeTask.new('style') do |task|
    task.options       = ['-c', 'config/rubocop.yml']
    task.fail_on_error = true
  end

  Reek::Rake::Task.new('maintainability') do |task|
    task.config_file   = 'config/reek.yml'
    task.source_files  = '**/*.rb'
    task.fail_on_error = true
  end
end

desc 'Run all linters'
task lint: [
  'lint:style',
  'lint:maintainability'
].map(&:to_sym)
