# frozen_string_literal: true

require 'rspec/core/rake_task'

RSpec::Core::RakeTask.new(:spec) do |t|
  t.rspec_path = 'bin/rspec'
end

namespace :spec do
  desc 'Run all specs and generate code coverage report'
  task :coverage do
    ENV['COVERAGE_REPORT'] = 'true'
    Rake::Task['spec'].invoke
  end
end
